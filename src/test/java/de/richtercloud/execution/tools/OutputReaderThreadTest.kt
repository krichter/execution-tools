/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.execution.tools

import org.apache.commons.exec.OS
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.lang.IllegalStateException
import java.util.*

internal class OutputReaderThreadTest {

    @Test
    fun testRunPiped() {
        val binaryPath: String
        if(OS.isFamilyUnix()) {
            binaryPath = ECHO_BINARY;
        }else {
            throw IllegalStateException(ONLY_UNIX_EXCEPTION_MESSAGE)
        }
        val process = ProcessBuilder(binaryPath, "abc")
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
        val processStdoutThread = OutputReaderThread(process.inputStream,
                OutputReaderThreadMode.PIPED)
        processStdoutThread.start()
        process.waitFor()
        processStdoutThread.join()
        val processStdout = IOUtils.toString(processStdoutThread.processOutputStream).trim()
        assertEquals("abc",
                processStdout)
    }

    @Test
    fun testRunOutputStream() {
        val binaryPath: String
        if(OS.isFamilyUnix()) {
            binaryPath = ECHO_BINARY;
        }else {
            throw IllegalStateException(ONLY_UNIX_EXCEPTION_MESSAGE)
        }
        val process = ProcessBuilder(binaryPath, "abc")
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
        val outputStream: ByteArrayOutputStream = ByteArrayOutputStream()
        val processStdoutThread = OutputReaderThread(process.inputStream,
                OutputReaderThreadMode.OUTPUT_STREAM,
                outputStream)
        processStdoutThread.start()
        process.waitFor()
        processStdoutThread.join()
        val processStdout = outputStream.toString().trim()
        assertEquals("abc",
                processStdout)
    }

    @Test
    fun testRunPipedNewline() {
        val binaryPath: String
        if(OS.isFamilyUnix()) {
            binaryPath = ECHO_BINARY;
        }else {
            throw IllegalStateException(ONLY_UNIX_EXCEPTION_MESSAGE)
        }
        val process = ProcessBuilder(binaryPath, "-e",
                "abc\\ndef")
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
        val processStdoutThread = OutputReaderThread(process.inputStream,
                OutputReaderThreadMode.PIPED)
        processStdoutThread.start()
        process.waitFor()
        processStdoutThread.join()
        val processStdout = IOUtils.toString(processStdoutThread.processOutputStream).trim()
        assertEquals("abc\ndef",
                processStdout)
    }

    @Test
    fun testRunOutputStreamNewline() {
        val binaryPath: String
        if(OS.isFamilyUnix()) {
            binaryPath = ECHO_BINARY;
        }else {
            throw IllegalStateException(ONLY_UNIX_EXCEPTION_MESSAGE)
        }
        val process = ProcessBuilder(binaryPath, "-e",
                "abc\\ndef")
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
        val outputStream: ByteArrayOutputStream = ByteArrayOutputStream()
        val processStdoutThread = OutputReaderThread(process.inputStream,
                OutputReaderThreadMode.OUTPUT_STREAM,
                outputStream)
        processStdoutThread.start()
        process.waitFor()
        processStdoutThread.join()
        val processStdout = outputStream.toString().trim()
        assertEquals("abc\ndef",
                processStdout)
    }

    @Test
    @Suppress("MagicNumber")
    fun testRunParallel() {
        val outputStream: PipedOutputStream = PipedOutputStream()
        val inputStream: PipedInputStream = PipedInputStream(outputStream)
        var inputThreadRunning: Boolean = true
        val randomStringBuilder: StringBuffer = StringBuffer(1024*1024)
            //is a buffer because StringBuffer is synchornized
        val inputThread: Thread = Thread {
            try {
                while (inputThreadRunning) {
                    val nextRandom: String = RandomStringUtils.randomAscii(1024)
                    outputStream.write(nextRandom.toByteArray())
                    randomStringBuilder.append(nextRandom)
                }
            }finally {
                outputStream.flush()
                outputStream.close()
            }
        }
        val outputStream0: ByteArrayOutputStream = ByteArrayOutputStream()
        val outputStream1: ByteArrayOutputStream = ByteArrayOutputStream()
        val outputStreams = LinkedList<OutputStream>(Arrays.asList(ByteArrayOutputStream(),
                outputStream0,
                outputStream1))
        val instance = OutputReaderThread(inputStream,
                OutputReaderThreadMode.OUTPUT_STREAM,
                outputStreams)
        instance.start()
        inputThread.start()
        Thread.sleep(5000)
        inputThreadRunning = false
        inputThread.join()
        instance.join()
        val randomString: String = randomStringBuilder.toString()
        assertEquals(randomString,
                outputStream0.toString().trim())
    }

    companion object {
        private const val ECHO_BINARY = "/bin/echo"
        private const val ONLY_UNIX_EXCEPTION_MESSAGE = "test only runs on UNIX-like OS"
    }
}
