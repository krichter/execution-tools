/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.execution.tools

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.util.Arrays
import java.util.LinkedList
import java.util.Scanner
import org.slf4j.LoggerFactory

/**
 *
 * @author richter
 */
@Suppress("CommentOverPrivateProperty")
class OutputReaderThread : Thread {
    /**
     * The process stream to read (`stdout` or `stderr`).
     */
    private val processStream: InputStream
    private val pipedInputStream = PipedInputStream()
    /**
     * The output stream to collect the output of the mapped process stream.
     * `null` indicates that the output ought to be redirected to console.
     *
     * Deadlocks which can occur when reading from the same thread according to
     * Javadoc cannot occur unless this property is exposed or
     * [.getProcessOutputStream] is called in this class.
     */
    /*
    - has ben `Appenadable`s before which is nonsense because callers can only
    retrieve the collected information afterwards by reading the console output
    in case it's redirected
    */
    private val pipedOutputStream = PipedOutputStream(pipedInputStream)
    val outputStreams: List<OutputStream>
    private val mode: OutputReaderThreadMode

    val processOutputStream: InputStream
        @Throws(IOException::class)
        get() = pipedInputStream

    @JvmOverloads constructor(processStream: InputStream,
                              mode: OutputReaderThreadMode = OutputReaderThreadMode.BOTH,
                              outputStream: OutputStream,
                              name: String = String.format("output-reader-thread-%d",
                                      nextCounter)) : this(processStream,
                mode,
                LinkedList(Arrays.asList(outputStream)),
                name)

    /**
     * Creates a new `OutputReaderThread`.
     *
     * @param processStream the process output stream to read from
     * @param outputStreams a list of output streams to collect the process stream output in (mustn't be {@code null},
     *     but can be empty)
     * @param mode whether to write to the specified output stream, the interal
     * piped output stream or both
     * @param name the name of the thread (see
     * [Thread.Thread] for details)
     * @throws IllegalArgumentException if {@code outputStreams} contains {@code null}
     */
    @JvmOverloads constructor(processStream: InputStream,
                              mode: OutputReaderThreadMode = OutputReaderThreadMode.BOTH,
                              outputStreams: List<OutputStream> = LinkedList(),
                              name: String = String.format("output-reader-thread-%d",
                                      nextCounter)) : super(name) {
        this.processStream = processStream
        this.mode = mode
        this.outputStreams = outputStreams
    }

    @Suppress("ComplexMethod", "NestedBlockDepth")
    override fun run() {
        //Using a BufferedReader buffering an InputStreamReader or the
        //InputStreamReader directly causes random deadlocks because of
        //blocks at BufferedReader.readLine or InputStreamReader.read after
        //Process.destory. Those simply don't ever happen with Scanner, so
        //Scanner is used. The tip comes from
        //https://stackoverflow.com/questions/30846870/java-problems-with-reading-from-process-output.
        //Using Scanner avoids the need to catch IOException and thus passing an
        //IssueHandler reference.
        val scanner = Scanner(processStream)
        //Possible condition to loop over (different have been tried because a
        //deadlock of InputStream.read occured after killing the postgres
        //process with Process.destroy; Scanner is the best because it avoids
        //catching IOException):
        //- testing for BufferedReader.ready causes thread to terminate
        //before the end of the output is reached
        //- testing for process.isAlive doesn't make sense because it discards
        //output after the process terminated
        //- using code from com.Ostermiller.util.ExecHelper causes output to be
        //not available in short runs like `[program] --version`
        //- Scanner.hasNext()
        while (scanner.hasNext()) {
            val line = scanner.nextLine()
            LOGGER.trace(String.format("[output reader] %s",
                    line))
            try {
                when (mode) {
                    OutputReaderThreadMode.OUTPUT_STREAM ->
                        for(outputStream in outputStreams) {
                            outputStream.write(line.toByteArray())
                            outputStream.write("\n".toByteArray())
                        }
                    OutputReaderThreadMode.PIPED -> {
                        pipedOutputStream.write(line.toByteArray())
                        pipedOutputStream.write("\n".toByteArray())
                    }
                    OutputReaderThreadMode.BOTH -> {
                        for(outputStream in outputStreams) {
                            outputStream.write(line.toByteArray())
                            outputStream.write("\n".toByteArray())
                        }
                        pipedOutputStream.write(line.toByteArray())
                        pipedOutputStream.write("\n".toByteArray())
                    }
                    else -> throw IllegalArgumentException(String.format("mode '%s' not supported",
                            mode))
                }
                //this might be handled more efficient, specially the switch
                //inside the while called for every line
            } catch (ex: IOException) {
                throw OutputTransmissionException(ex)
            }

        }
        pipedOutputStream.flush()
        pipedOutputStream.close()
        LOGGER.trace("output reader thread terminated")
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(OutputReaderThread::class.java)
        /**
         * Counter used to create meaningful thread names. Thread names might be
         * used by callers already.
         */
        private var counter: Int = 0

        @Suppress("UselessPostfixExpression") // false positive
        private val nextCounter: Int
            get() = synchronized(OutputReaderThread::class.java) {
                return counter++
            }
    }
}
