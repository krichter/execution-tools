/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.execution.tools

import com.google.common.collect.ImmutableMap
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.lang.ProcessBuilder.Redirect
import java.util.Arrays
import org.apache.commons.lang3.tuple.ImmutableTriple
import org.apache.commons.lang3.tuple.Triple
import org.slf4j.LoggerFactory

/**
 *
 * @author richter
 */
@Suppress("LongParameterList")
object ExecutionUtils {
    private val LOGGER = LoggerFactory.getLogger(ExecutionUtils::class.java)
    const val SH_DEFAULT = "sh"
    private const val PATH_TEMPLATE = "PATH"

    @JvmStatic
    @Throws(IOException::class)
    fun createProcess(directory: File? = null,
                      sh: String = SH_DEFAULT,
                      stdoutOutputStream: OutputStream,
                      stderrOutputStream: OutputStream,
                      outputReaderThreadMode: OutputReaderThreadMode,
                      vararg commands: String): Triple<Process, OutputReaderThread, OutputReaderThread> {
        return createProcess(directory,
                System.getenv(PATH_TEMPLATE),
                sh,
                listOf(stdoutOutputStream),
                listOf(stderrOutputStream),
                outputReaderThreadMode,
                *commands)
    }

    @JvmStatic
    @Throws(IOException::class)
    fun createProcess(directory: File,
                      sh: String = SH_DEFAULT,
                      stdoutOutputStreams: List<OutputStream>,
                      stderrOutputStreams: List<OutputStream>,
                      outputReaderThreadMode: OutputReaderThreadMode,
                      vararg commands: String): Triple<Process, OutputReaderThread, OutputReaderThread> {
        return createProcess(directory,
                System.getenv(PATH_TEMPLATE),
                sh,
                stdoutOutputStreams,
                stderrOutputStreams,
                outputReaderThreadMode,
                *commands)
    }

    @JvmStatic
    @Throws(IOException::class)
    fun createProcess(directory: File? = null,
                      path: String,
                      sh: String = SH_DEFAULT,
                      stdoutOutputStreams: List<OutputStream>,
                      stderrOutputStreams: List<OutputStream>,
                      outputReaderThreadMode: OutputReaderThreadMode,
                      vararg commands: String): Triple<Process, OutputReaderThread, OutputReaderThread> {
        return createProcess(directory,
                ImmutableMap.builder<String, String>()
                        .put(PATH_TEMPLATE, path)
                        .build(),
                sh,
                stdoutOutputStreams,
                stderrOutputStreams,
                outputReaderThreadMode,
                *commands)
    }

    /**
     * Allows sharing code between different process creation routines.
     *
     * @param directory the directory in which to execute the process
     * @param env the environment mapping
     * @param sh the shell in which to wrap the command execution
     * @param stdoutOutputStream the output stream to collect the stdout output
     * @param stderrOutputStream the output stream to collect the stderr output
     * @param commands the commands to create a [Process] for
     * @return the created process and references to the
     * [OutputReaderThread] for `stdout` and `stderr`
     * depending on whether `stdoutStream` or `stderrStream`
     * have been not `null`
     * @throws IOException if an I/O exception occurs during process
     * communication
     */
    /*
    internal implementation notes:
    - checking for canceled doesn't make sense here because null would have to
    be returned or an exception thrown in the case of canceled state which
    creates the need to evaluate the return value by callers which is equally
    complex as checking the condition before calls to createProcess
    */
    @JvmStatic
    @Throws(IOException::class)
    fun createProcess(directory: File? = null,
                      env: Map<String, String>,
                      sh: String = SH_DEFAULT,
                      stdoutOutputStreams: List<OutputStream>,
                      stderrOutputStreams: List<OutputStream>,
                      outputReaderThreadMode: OutputReaderThreadMode,
                      vararg commands: String): Triple<Process, OutputReaderThread, OutputReaderThread> {
        LOGGER.trace(String.format("building process with commands '%s' with environment '%s' running in %s",
                Arrays.asList(*commands),
                env,
                if (directory != null) {
                    String.format("directory '%s'",
                            directory.absolutePath)
                } else {
                    "current directory"
                }))
        val processBuilder = ProcessBuilder(sh, "-c", commands.joinToString(" "))
                .redirectOutput(Redirect.PIPE)
                .redirectError(Redirect.PIPE)
            //always redirect/pipe to specified streams because writing to stdout or stderr is handled by specifying an
            //output stream
        //need to wrap commands in a shell in order allow modified path for
        //binary discovery (the unmodifiable PATH of the JVM is used to find
        //the command to execute which doesn't allow any modification after
        //installations by wrapper)
        if (directory != null) {
            processBuilder.directory(directory)
        }
        processBuilder.environment().putAll(env)
        val retValue = processBuilder.start()
        var stdoutReaderThread: OutputReaderThread? = null
        var stderrReaderThread: OutputReaderThread? = null
        stdoutReaderThread = OutputReaderThread(retValue.inputStream,
                outputReaderThreadMode,
                stdoutOutputStreams)
        stdoutReaderThread.start()
        stderrReaderThread = OutputReaderThread(retValue.errorStream,
                outputReaderThreadMode,
                stderrOutputStreams)
        stderrReaderThread.start()
        return ImmutableTriple(retValue,
                stdoutReaderThread,
                stderrReaderThread)
    }
}
